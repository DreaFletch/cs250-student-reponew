#include <iostream>
using namespace std;

#include "UTILITIES/Menu.hpp"

#include "CourseCatalog.hpp"

int main()
{
    CourseCatalog courseProgram;
    courseProgram.Run();

    return 0;
}
